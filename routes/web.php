<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/test/{angka}', function ($angka) {
//     return view('test', ["angka" => $angka]);
// });

// // Route::get('user/{id}', function ($id) {
// //     return 'Hai '.$id;
// // });

// Route::get('/form', 'RegisterController@form');
// Route::get('/sapa', 'RegisterController@sapa');
// Route::post('/sapa', 'RegisterController@sapa_post');

// Route::get('/', 'HomeController@Home');

// Route::get('/register', 'AuthController@Register');

// Route::post('/welcome', 'AuthController@Welcome');

Route::get('/master', function() {
    return view('/adminlte/master');
});

// Route::get('/items', function() {
//    return view('items.index'); 
// });

// Route::get('/items/create', function() {
//     return view('items.create'); 
//  });

Route::get('/table', function() {
    return view('tasks.showtable'); 
 });

 Route::get('/data-table', function() {
    return view('tasks.showdatatable'); 
 });
